﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class MyList<T>
    {
        private T[] array = new T[10];
        private int arrayLength = 10;
        public int Count { get; private set; } = 0;

        public MyList() 
        {

        }

        public void Add(T item)
        {
            if(arrayLength - Count < 2)
            {
                arrayLength *= 2;
                var oldArray = array;
                array = new T[arrayLength];
                oldArray.CopyTo(array, 0);
            }
            array[Count] = item;
            Count++;
        }

        public void Clear()
        {
           
        }

        public void Remove(T item)
        {

        }

        public T[] ToArray()
        {
            return array;
        }

    }
}
