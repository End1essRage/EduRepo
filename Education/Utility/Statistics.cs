﻿using System.Text;

namespace Utility
{
    public class Statistics
    {
        public long ElapsedMs { get; set; }
        public long CompairsAmount { get; set; }
        public long AssignsAmount { get; set; }
        public Statistics()
        {
            ElapsedMs = 0;
            CompairsAmount = 0;
            AssignsAmount = 0;
        }

        public string GetInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Elapsed Time is " + ElapsedMs + "\n");
            sb.Append("Compairs Amount is " + CompairsAmount + "\n");
            sb.Append("Assigns Amount is " + AssignsAmount + "\n");

            return sb.ToString();
        }
    }
}