﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public static class Utility
    {
        public static int[] GetRandomIntArray(int lenght, int minValue, int maxValue)
        {
            Random rnd = new Random();
            int[] array = new int[lenght];
            for(int i = 0; i < lenght; i++)
            {
                array[i] = rnd.Next(minValue, maxValue);
            }
            return array;
        }
    }
}
