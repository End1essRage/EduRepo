﻿using System.Diagnostics;
using Utility;

namespace Algorithms
{
    public static class Sort
    {
        public static int[] BubbleSort(int[] array, out Statistics stat)
        {
            stat = new Statistics();
            bool flag = true;
            var stopWatch = Stopwatch.StartNew();
            
            while (flag)
            {
                flag = false;
                for (int i = 1; i < array.Length; i++)
                {
                    int a = 0;
                    stat.CompairsAmount++;
                    if (array[i] < array[i - 1])
                    {
                        a = array[i];
                        array[i] = array[i - 1];
                        array[i - 1] = a;
                        stat.AssignsAmount += 3;
                        flag = true;
                    }
                }
            }

            stat.ElapsedMs = stopWatch.ElapsedMilliseconds;
            stopWatch.Stop();
            return array;
        }

    }
}