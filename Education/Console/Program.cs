﻿using Algorithms;
using System;
using System.Reflection.Metadata.Ecma335;
using System.Security.Principal;
using Utility;
using Patterns;
using Patterns.Decorator;
using DataStructures;

namespace EduConsole
{
    public static class Program
    {
        static void Main(string[] args)
        {
            #region Strategy Example
            /*
            Window simpleWindow = new Window(new SimpleDisplayStrategy());
            Window devWindow = new Window(new DevelopmentDisplayStrategy());

            simpleWindow.Display();
            devWindow.Display();
            */
            #endregion
            #region Decorator Example
            /*
            ConcreteComponent component = new ConcreteComponent();
            //Add functionallity after executing component method
            DecoratorA decoratorA1 = new DecoratorA(component);
            //Add functionallity before executing component method
            DecoratorB decoratorB1 = new DecoratorB(component);
            //override component methods
            DecoratorC decoratorC1 = new DecoratorC(component);
            //We can decorate multiple times
            DecoratorA decoratorA2 = new DecoratorA(component);
            DecoratorA decoratorA3 = new DecoratorA(decoratorA2);
            */
            #endregion

            #region MyListTests
            /*
            MyList<int> myList = new MyList<int>();
            for(int i = 0; i < 20;i++)
            {
                myList.Add(i);
            }
            var arr = myList.ToArray();
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine(arr[i]);
            }
            */
            #endregion
        }

        static Statistics GetBubbleSortStat(int[] array)
        {
            Statistics stat = new Statistics();

            array = Sort.BubbleSort(array, out stat);
            return stat;
        }
    }
}