﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Decorator
{
    public class DecoratorA : DecoratorBase
    {
        public DecoratorA(ComponentBase component) : base(component) {}
        public override void DoSmth()
        {
            base.DoSmth();

            Console.WriteLine("Decorator a");
        }
    }
}
