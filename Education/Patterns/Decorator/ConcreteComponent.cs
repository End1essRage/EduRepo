﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Decorator
{
    public class ConcreteComponent : ComponentBase
    {
        public override void DoSmth()
        {
            Console.WriteLine("IAmComponent");
        }
    }
}
