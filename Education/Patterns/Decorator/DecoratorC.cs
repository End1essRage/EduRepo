﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Decorator
{
    public class DecoratorC : DecoratorBase
    {
        public DecoratorC(ComponentBase component) : base(component) {}

        public override void DoSmth()
        {
            Console.WriteLine("Decorator c");
        }
    }
}
