﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Decorator
{
    public abstract class DecoratorBase : ComponentBase
    {
        public ComponentBase component { get; set; }
        public DecoratorBase(ComponentBase component) 
        {
            this.component = component;
        }

        public override void DoSmth()
        {
            component.DoSmth();
        }
    }
}
