﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Strategy
{
    public class Window
    {
        private IDisplayStrategy _displayStrategy;

        public Window(IDisplayStrategy displayStrategy)
        {
            _displayStrategy = displayStrategy;
        }

        public void Display()
        {
            _displayStrategy.Display();
        }
    }
}
