﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Strategy
{
    public class DevelopmentDisplayStrategy : IDisplayStrategy
    {
        public void Display()
        {
            Console.WriteLine("its a development displaying");
        }
    }
}
