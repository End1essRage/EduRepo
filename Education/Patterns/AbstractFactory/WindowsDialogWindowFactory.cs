﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.AbstractFactory
{
    public class WindowsDialogWindowFactory : IDialogWindowAbstractFactory
    {
        public IButton GetButton()
        {
            return new WindowsButton();
        }

        public ITextBox GetTextBox()
        {
            return new WindowsTextBox();
        }
    }
}
