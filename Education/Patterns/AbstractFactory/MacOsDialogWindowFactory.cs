﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.AbstractFactory
{
    public class MacOsDialogWindowFactory : IDialogWindowAbstractFactory
    {
        public IButton GetButton()
        {
            return new MacOsButton();
        }

        public ITextBox GetTextBox()
        {
            return new MacOsTextBox();
        }
    }
}
