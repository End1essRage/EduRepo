﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.AbstractFactory
{
    public class MacOsButton : IButton
    {
        public string Name { get; set; } = "macos button";

        public void OnPress()
        {
            Console.WriteLine("macos button pressed");
        }
    }
}
