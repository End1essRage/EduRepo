﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.AbstractFactory
{
    public class WindowsButton : IButton
    {
        public string Name { get; set; } = "windows button";

        public void OnPress()
        {
            Console.WriteLine("windows button pressed");
        }
    }
}
