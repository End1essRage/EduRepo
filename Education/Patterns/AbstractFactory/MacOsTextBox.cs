﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.AbstractFactory
{
    public class MacOsTextBox : ITextBox
    {
        public string Text { get; set; } = "macos textbox";
    }
}
