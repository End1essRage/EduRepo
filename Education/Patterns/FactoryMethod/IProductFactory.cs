﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.FactoryMethod
{
    public interface IProductFactory
    {
        public IProduct GetProduct(string productName);
    }
}
