﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.FactoryMethod
{
    public class SimpleProductFactory : IProductFactory
    {
        public IProduct GetProduct(string productName)
        {
            var product = new Product();
            product.Name = productName;
            return product;
        }
    }
}
